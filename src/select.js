import { pick } from './pick';

export const selectObjects = (arr, keys) => {
  const result = [];
  for (const k of arr) {
    result.push(pick(k, keys));
  }
  return result;
};

export const select = (arr, keys) => arr.map(obj => pick(obj, keys));
