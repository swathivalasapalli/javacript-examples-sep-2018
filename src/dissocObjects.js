export const dissoc = (key, obj) => {
  const o = {};
  for (const k in obj) {
    if (obj.hasOwnProperty(k)) {
      if (k !== key) {
        o[k] = obj[k];
      }
    }
  }
  return o;
};
