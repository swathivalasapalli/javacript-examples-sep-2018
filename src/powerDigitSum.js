export const power = (x, n) => {
  let pow = 1;
  for (let i = 0; i < n; i += 1) {
    pow *= x;
  }
  return pow;
};

export const sumOfIndividualDigits = n => {
  let sum = 0;
  while (n > 0) {
    sum += Math.floor(n % 10);
    n /= 10;
  }
  return sum;
};

export const powerDigitSum = (x, n) => sumOfIndividualDigits(power(x, n));
