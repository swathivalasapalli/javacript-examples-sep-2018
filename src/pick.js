export const pickSingle = (obj, key) => {
  const result = {};
  for (const k in obj) {
    if (k === key) {
      result[k] = obj[k];
    }
  }
  return result;
};

export const pick = (obj, keys) => {
  const result = {};
  for (const k of keys) {
    result[k] = obj[k];
  }
  return result;
};
