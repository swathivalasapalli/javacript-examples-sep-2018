export const rangeForPositive = (start, stop, step) => {
  const result = [];
  for (let i = start; i < stop; i += step) {
    result.push(i);
  }
  return result;
};

export const sumOfArrayElements = arr => {
  let sum = 0;
  for (const i of arr) {
    sum += i;
  }
  return sum;
};

export const sumOfRange = arr => sumOfArrayElements(rangeForPositive(arr));
