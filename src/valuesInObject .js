export const values = obj => {
  const o = [];
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      o.push(obj[key]);
    }
  }
  return o;
};
