export const whereForSingleObject = (obj, predicate) => {
  const o = {};
  for (const k in obj) {
    if (obj.hasOwnProperty(k) && predicate(obj[k])) {
      o[k] = obj[k];
    }
  }
  return o;
};

export const whereObjects = (arr, predicate) => {
  const result = [];
  for (const k of arr) {
    result.push(whereForSingleObject(k, predicate));
  }
  return result;
};

export const where = (arr, predicate) =>
  arr.map(obj => whereForSingleObject(obj, predicate));
