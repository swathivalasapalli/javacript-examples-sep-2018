import { power } from './powerDigitSum';

export const distinctElements = arr => {
  const result = [];
  for (let i = 0; i < arr.length; i += 1) {
    if (result.indexOf(arr[i]) === -1) {
      result.push(arr[i]);
    }
  }
  return result;
};

export const allPowers = (a, b) => {
  const result = [];
  for (let i = a; i <= b; i += 1) {
    for (let j = a; j <= b; j += 1) {
      result.push(power(i, j));
    }
  }
  return result;
};

export const distinctPowers = (a, b) => distinctElements(allPowers(a, b));

export const countDistictPowers = (a, b) => distinctPowers(a, b).length;
