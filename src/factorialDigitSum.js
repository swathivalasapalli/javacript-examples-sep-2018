import { sumOfIndividualDigits } from './powerDigitSum';

export const factorial = n => {
  let fact = 1;
  for (let i = 1; i <= n; i += 1) {
    fact *= i;
  }
  return fact;
};

export const factorialDigitSum = n => sumOfIndividualDigits(factorial(n));
