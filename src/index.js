import { range } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import {
  createDeck,
  shuffleDeck,
  getNextcard,
  createDeckObjects,
  getCardValue,
} from './blackJackgame';

import { sumOfMultiples } from './sumOfMultiples';

import { lcm, max, smallestInteger } from './smallestPositiveNumber';
import {
  isPrime,
  factors,
  primeFactors,
  largest,
  largestPrimeFactor,
} from './primeFactors';

import { filterPrimeFactors } from './prime';

import { sumOfSquares, squaresOfSum } from './sumSquareDifference';
import { primesUpToNumber } from './findPrimeNumberAtIndex';

import { findPrimeNumber } from './findPrimeNumber';
import { power, sumOfIndividualDigits, powerDigitSum } from './powerDigitSum';
import { factorial, factorialDigitSum } from './factorialDigitSum';
import {
  fibonacci,
  sumOfEvenNumbers,
  evenFibonacciNumbers,
} from './evenFibonacciNumbers';

import {
  reverse,
  isPalindrome,
  largestPalindromeProduct,
} from './palindromeProduct';

import {
  allPowers,
  distinctElements,
  distinctPowers,
  countDistictPowers,
} from './distinctPowers';
import { countB, countChar } from './countB';
import { isEven } from './isEven';
import { rangeForPositive, sumOfArrayElements } from './range';
import { reverseArray, reverseArrayInplace } from './reverseArray';
import { mergeObjects } from './merge';
import { invertObj } from './invertObjects';
import { values } from './valuesInObject ';
import { keys } from './keysInObject';
import { clone, assoc } from './assocObjects';
import { dissoc } from './dissocObjects';
import { pickSingle, pick } from './pick';
import { omit, omitSingle } from './omitObjects';
import { mergeArrays, sort, mergeSort } from './mergeSort';
import { select, selectObjects } from './select';
import { whereForSingleObject, where, whereObjects } from './where';

range(1, 20)
  .pipe(
    filter(x => x % 2 === 1),
    map(x => x * x),
  )
  .subscribe(x => console.log(x));

console.log(createDeckObjects());
console.log(createDeck());
const deck = createDeck();
console.log(shuffleDeck(deck));
console.log(getNextcard());
console.log(getNextcard());
console.log(getCardValue('Seven'));
console.log(sumOfMultiples(1000));
console.log(max(4, 100));
console.log(lcm(1, 5));
console.log(smallestInteger(10));
console.log(smallestInteger(20));
console.log(isPrime(8));
console.log(factors(8));
console.log(primeFactors(13195));
console.log(largest([1, 4, 10, 3]));
console.log(largestPrimeFactor(13195));
console.log(filterPrimeFactors(13195));
console.log(sumOfSquares(10));
console.log(squaresOfSum(10));
console.log(squaresOfSum(10) - sumOfSquares(10));
console.log(primesUpToNumber(15));
console.log(findPrimeNumber(6));
console.log(power(2, 15));
console.log(sumOfIndividualDigits(32768));
console.log(sumOfIndividualDigits(power(2, 15)));
console.log(powerDigitSum(2, 15));
console.log(powerDigitSum(2, 1000));
console.log(factorial(5));
console.log(factorialDigitSum(10));
console.log(factorialDigitSum(100));
console.log(fibonacci(10));
console.log(sumOfEvenNumbers([1, 2, 4]));
console.log(evenFibonacciNumbers(10));
console.log(reverse(123));
console.log(isPalindrome(999));
console.log(isPalindrome(199));
console.log(distinctElements([1, 2, 3, 1, 3, 5]));
console.log(distinctElements(allPowers(2, 5)));
console.log(distinctPowers(2, 5));
console.log(countDistictPowers(2, 5));
console.log(largestPalindromeProduct());
console.log(countB('BABYb', 'B'));
console.log(countChar('javascriptgoodparts', 'a'));
console.log(isEven(-1));
console.log(isEven(50));
console.log(rangeForPositive(1, 10, 2));
console.log(reverseArray([1, 2, 3, 4]));
console.log(reverseArrayInplace([1, 2, 3, 4, 5, 6, 8, 10]));
console.log(sumOfArrayElements(rangeForPositive(1, 10, 2)));
console.log(
  mergeObjects(
    { name: 'swathi', branch: 'cse' },
    { company: 'technoidentity' },
  ),
);

const props = { first: 'alice', second: 'jake' };
console.log(invertObj(props));
console.log(values(props));
console.log(keys(props));
console.log(clone(props));
console.log(assoc('middle', 'swathi', props));
console.log(dissoc('second', props));
console.log(pickSingle({ a: 1, b: 2, c: 3, d: 4 }, 'a'));
console.log(omitSingle('a', { a: 1, b: 2, c: 3, d: 4 }));
console.log(omit({ a: 1, b: 2, c: 3, d: 4 }, ['a', 'd']));
console.log(pick({ a: 1, b: 2, c: 3, d: 4 }, ['a', 'b']));
console.log(mergeObjects({ name: 'swathi', age: 20 }, { age: 30 }));
console.log(mergeArrays([1, 5, 6, 10], [2, 4, 7, 8]));
console.log(
  select(
    [{ x: 1, y: 2, z: 3 }, { x: 5, y: 6, z: 7 }, { x: 9, y: 10, z: 20 }],
    ['x', 'y'],
  ),
);
console.log(whereForSingleObject({ x: 1, y: 2, z: 3 }, values => values >= 2));
console.log(
  where([{ x: 1, y: 2, z: 3 }, { x: 5, y: 6, z: 3 }], values => values > 4),
);

console.log(
  selectObjects(
    [{ x: 1, y: 2, z: 3 }, { x: 5, y: 6, z: 7 }, { x: 9, y: 10, z: 20 }],
    ['x', 'y'],
  ),
);
console.log(
  whereObjects(
    [{ x: 1, y: 2, z: 3, age: 30 }, { x: 5, y: 6, z: 3, age: 45 }],
    age => age > 30,
  ),
);
console.log(sort([1, 5, 4, 11, 6, 2, 8, 10, 9]));
console.log(mergeSort([1, 5, 6, 14, 10, 2, 4, 7, 11, 8, 9]));
