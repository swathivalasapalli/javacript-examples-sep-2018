export const omitSingle = (key, obj) => {
  const { [key]: _, ...rest } = obj;
  return rest;
};

export const omit = (obj, keys) => {
  const result = {};
  for (const k in obj) {
    if (obj.hasOwnProperty(k) && keys.indexOf(k) === -1) {
      result[k] = obj[k];
    }
  }
  return result;
};
