export const reverse = n => {
  let rev = 0;
  while (n > 0) {
    const remainder = n % 10;
    rev = rev * 10 + remainder;
    n = Math.floor(n / 10);
  }
  return rev;
};

export const isPalindrome = n => reverse(n) === n;

export const largestPalindromeProduct = () => {
  let largest = 0;
  const max = 999;
  const min = 100;
  for (let i = min; i <= max; i += 1) {
    for (let j = min; j <= max; j += 1) {
      const product = i * j;
      if (isPalindrome(product) && product > largest) {
        largest = product;
      }
    }
  }
  return largest;
};
