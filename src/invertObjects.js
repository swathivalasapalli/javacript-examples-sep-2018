export const invertObj = obj => {
  const o = {};
  for (const prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      o[obj[prop]] = prop;
    }
  }
  return o;
};
