export const max = (x, y) => {
  if (x > y) {
    return x;
  }
  return y;
};

export const lcm = (x, y) => {
  let m = max(x, y);
  while (true) {
    if (m % x === 0 && m % y === 0) {
      return m;
    }
    m += 1;
  }
};

export const smallestInteger = n => {
  let min = 1;
  for (let i = 1; i <= n; i += 1) {
    min = lcm(min, i);
  }
  return min;
};
