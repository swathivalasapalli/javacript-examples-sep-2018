import { isPrime } from './primeFactors';

export const sumOfPrimeNumbers = n => {
  let sum = 0;
  for (let i = 2; i <= n; i += 1) {
    if (isPrime(i)) {
      sum += i;
    }
  }
  return sum;
};
