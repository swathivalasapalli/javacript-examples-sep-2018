export const reverseArray = arr => {
  const result = [];
  for (let i = arr.length - 1; i > 0; i -= 1) {
    result.push(i);
  }
  return result;
};

export const reverseArrayInplace = arr => {
  for (let i = 0; i < Math.floor(arr.length / 2); i += 1) {
    const temp = arr[i];
    arr[i] = arr[arr.length - 1 - i];
    arr[arr.length - 1 - i] = temp;
  }
  return arr;
};
