export const keys = obj => {
  const o = [];
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      o.push(key);
    }
  }
  return o;
};
