const suits = ['hearts', 'diamonds', 'clubs', 'spades'];
const values = [
  'Ace',
  'Jack',
  'King',
  'Queen',
  'Ten',
  'Nine',
  'Eight',
  'Seven',
  'Six',
  'Five',
  'Four',
  'Three',
  'Two',
];

export const createDeck = () => {
  const deck = [];
  for (let i = 0; i < suits.length; i += 1) {
    for (let j = 0; j < values.length; j += 1) {
      deck.push(`${values[j]} of ${suits[i]}`);
    }
  }
  return deck;
};

export const shuffleDeck = deck => {
  for (let i = 0; i < deck.length; i += 1) {
    const shuffleindex = Math.floor(Math.random() * 52);
    const temp = deck[shuffleindex];
    deck[shuffleindex] = deck[i];
    deck[i] = temp;
  }
  return deck;
};

export const createDeckObjects = () => {
  const deck = [];
  for (let i = 0; i < suits.length; i += 1) {
    for (let j = 0; j < values.length; j += 1) {
      const card = {
        suit: suits[i],
        value: values[j],
      };
      deck.push(card);
    }
  }
  return deck;
};

const deck = createDeck();
export const getNextcard = () => deck.shift();

export const getCardValue = card => {
  switch (card.value) {
    case 'Ace':
      return 1;
    case 'Two':
      return 2;
    case 'Three':
      return 3;
    case 'Four':
      return 4;
    case 'Five':
      return 5;
    case 'Six':
      return 6;
    case 'Seven':
      return 7;
    case 'Eight':
      return 8;
    case 'Nine':
      return 9;
    default:
      return 10;
  }
};
