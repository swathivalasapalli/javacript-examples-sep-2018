export const fibonacci = n => {
  const result = [1, 2];
  for (let i = 2; i < n; i += 1) {
    result.push(result[i - 1] + result[i - 2]);
  }
  return result;
};

export const sumOfEvenNumbers = arr => {
  let sum = 0;
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i] % 2 === 0) {
      sum += arr[i];
    }
  }
  return sum;
};

export const evenFibonacciNumbers = n => sumOfEvenNumbers(fibonacci(n));
