import { isPrime } from './primeFactors';

export const findPrimeNumber = n => {
  let countofprimes = 0;
  let prime = 1;
  while (countofprimes < n) {
    prime += 1;
    if (isPrime(prime)) {
      countofprimes += 1;
    }
  }
  return prime;
};
