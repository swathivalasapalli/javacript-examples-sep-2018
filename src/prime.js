import { isPrime, factors } from './primeFactors';

export const filterPrimeFactors = n => {
  const result = [];
  for (let i = 1; i < factors(n).length; i += 1) {
    if (isPrime(factors(n)[i])) {
      result.push(factors(n)[i]);
    }
  }
  return result;
};
