export const countB = (str, c) => {
  let count = 0;
  for (let i = 0; i < str.length; i += 1) {
    if (str[i] === c) {
      count += 1;
    }
  }
  return count;
};

export const countChar = (str, c) => countB(str, c);
