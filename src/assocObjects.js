export const clone = obj => {
  const o = {};
  for (const prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      o[prop] = obj[prop];
    }
  }
  return o;
};

export const assoc = (key, value, obj) => {
  const o = clone(obj);
  o[key] = value;
  return o;
};
