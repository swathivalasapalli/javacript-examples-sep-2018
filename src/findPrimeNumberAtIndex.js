import { isPrime } from './primeFactors';

export const primesUpToNumber = n => {
  const result = [];
  for (let i = 2; i <= n; i += 1) {
    if (isPrime(i)) {
      result.push(i);
    }
  }
  return result;
};

export const findValueAtIndex = (arr, index) => {
  for (let i = 1; i < arr.length; i += 1) {
    if (i === index) {
      return arr[i];
    }
  }
  return -1;
};
