export const isPrime = n => {
  for (let i = 2; i < n; i += 1) {
    if (n % i === 0) {
      return false;
    }
  }
  return true;
};

export const factors = n => {
  const result = [];
  for (let i = 1; i <= n; i += 1) {
    if (n % i === 0) {
      result.push(i);
    }
  }
  return result;
};

export const primeFactors = n => {
  const result = [];
  for (let i = 2; i <= n; i += 1) {
    if (n % i === 0 && isPrime(i)) {
      result.push(i);
    }
  }
  return result;
};

export const largest = arr => {
  let max = arr[0];
  for (let i = 1; i < arr.length; i += 1) {
    if (arr[i] > max) {
      max = arr[i];
    }
  }
  return max;
};

export const largestPrimeFactor = n => {
  const result = [];
  for (let i = 2; i <= n; i += 1) {
    if (n % i === 0 && isPrime(i)) {
      result.push(i);
    }
  }
  return largest(result);
};
